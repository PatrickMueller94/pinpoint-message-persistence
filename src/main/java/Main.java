import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import interfaces.IKafkaConstants;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import pojos.PinpointMessage;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

/**
 * Created by student on 26.04.19.
 */
public class Main
{
    public static void main( String[] args )
    {
        System.out.println( "Start Consumer" );

        runConsumer( );
    }

    private static void runConsumer( )
    {
        Properties props = new Properties( );
        props.setProperty( ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, IKafkaConstants.KAFKA_BROKERS );
        props.setProperty( ConsumerConfig.GROUP_ID_CONFIG, IKafkaConstants.GROUP_ID_CONFIG );
        props.setProperty( ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true" );
        props.setProperty( ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100" );
        props.setProperty( ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName( ) );
        props.setProperty( ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName( ) );

        KafkaConsumer< String, String > consumer = new KafkaConsumer<>( props );
        consumer.subscribe( Arrays.asList( IKafkaConstants.PINPOINT_TOPIC ) );

        while ( true )
        {
            ConsumerRecords< String, String > records = consumer.poll( Duration.ofMillis( 100 ) );

            for ( ConsumerRecord< String, String > record : records )
            {
                //System.out.printf( "offset = %d, key = %s, value = %s%n", record.offset( ), record.key( ), record
                //        .value( ) );

                PinpointMessage pinpointMessage = deserializeMessage( record.value() );

                System.out.printf( "offset = %d, value = %s%n", record.offset( ), pinpointMessage.toString( ) );
            }
        }
    }

    private static PinpointMessage deserializeMessage( final String jsonString )
    {
        Gson gson = new GsonBuilder( ).setPrettyPrinting( ).create( );

        return gson.fromJson( jsonString, PinpointMessage.class );
    }
}