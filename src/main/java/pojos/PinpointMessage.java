package pojos;

/**
 * Created by student on 27.04.19.
 */
public class PinpointMessage
{
    private String message;

    public PinpointMessage( )
    {
        this.message = "";
    }

    public String getMessage( )
    {
        return message;
    }

    public void setMessage( final String message )
    {
        this.message = message;
    }

    @Override
    public String toString( )
    {
        return "PinpointMessage( " + this.message + " )";
    }
}