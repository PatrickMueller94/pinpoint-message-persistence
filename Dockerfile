FROM openjdk:12-alpine

COPY target/pinpoint-message-persistence-*.jar /target/pinpoint-message-persistence.jar
COPY target/classes /target/classes

CMD ["java", "-jar", "/target/pinpoint-message-persistence.jar"]